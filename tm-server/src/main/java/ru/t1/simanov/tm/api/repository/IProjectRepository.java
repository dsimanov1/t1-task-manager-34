package ru.t1.simanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

    Project create(
            @NotNull String userId,
            @NotNull String name
    );

}
