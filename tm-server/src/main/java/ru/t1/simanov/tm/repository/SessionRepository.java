package ru.t1.simanov.tm.repository;

import ru.t1.simanov.tm.api.repository.ISessionRepository;
import ru.t1.simanov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
