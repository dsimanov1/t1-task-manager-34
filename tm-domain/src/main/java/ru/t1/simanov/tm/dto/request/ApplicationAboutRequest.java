package ru.t1.simanov.tm.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ApplicationAboutRequest extends AbstractRequest {
}
